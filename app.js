const express = require("express");
const app = express();
const ExpressError = require("./expressError");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const calculateMean = (numbers) => {
    const sum = numbers.reduce((acc, number) => acc + number, 0);
    return sum / numbers.length;
};

const calculateMedian = (numbers) => {
    const sortedNums = [...numbers].sort((a, b) => a - b);
    const midIndex = Math.floor(sortedNums.length / 2);
    const isEvenLength = sortedNums.length % 2 === 0;

    return isEvenLength ? (sortedNums[midIndex - 1] + sortedNums[midIndex]) / 2 : sortedNums[midIndex];
};

const calculateMode = (numbers) => {
    const freqCounter = new Map();
    let maxFrequency = 0;
    let modes = [];

    numbers.forEach(num => {
        const key = Number(num);
        const count = (freqCounter.get(key) || 0) + 1;
        freqCounter.set(key, count);
        if (count > maxFrequency) {
            modes = [key];
            maxFrequency = count;
        } else if (count === maxFrequency) {
            modes.push(key);
        }
    });

    return modes.length === 1 ? modes[0] : modes;
};

const validateNumsQuery = (req, res, next) => {
    try {
        console.log("Original query nums:", req.query.nums);

        if (!req.query.nums) {
            throw new ExpressError("No nums provided", 400);
        }

        const nums = req.query.nums.split(",").map(Number);
        console.log("Parsed nums:", nums);

        const invalidNums = nums.filter((num) => Number.isNaN(num));

        if (invalidNums.length) {
            throw new ExpressError(
                `Invalid nums: ${invalidNums.join(", ")}`,
                400
            );
        }

        req.nums = nums;
        console.log("Valid nums:", req.nums);

        next();
    } catch (err) {
        next(err);
    }
};


app.get("/mean", validateNumsQuery, (req, res) => {
    const mean = calculateMean(req.nums);
    res.json({
        response: {
            operation: "mean",
            value: mean,
        },
    });
});

app.get("/median", validateNumsQuery, (req, res) => {
    const median = calculateMedian(req.nums);
    res.json({
        response: {
            operation: "median",
            value: median,
        },
    });
});

app.get("/mode", validateNumsQuery, (req, res) => {
    console.log("#### Original query nums:", req.query.nums);
    try {
        const mode = calculateMode(req.nums);
        res.json({
            response: {
                operation: "mode",
                value: mode,
            },
        });
    } catch (err) {
        // Handle any errors that occur during mode calculation
        res.status(500).json({ error: err.message });
    }
});


app.use((req, res, next) => {
    const e = new ExpressError("Page Not Found", 404)
    next(e)
})

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: err,
        message: err.message,
    });
});

app.listen(3001, () => {
    console.log("Server started on port 3001");
});

module.exports = {
    calculateMean,
    calculateMedian,
    calculateMode
}
