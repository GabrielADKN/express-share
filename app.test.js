const { calculateMean, calculateMedian, calculateMode } = require('./app.js');

describe('calculateMean', () => {
    test('calculates mean correctly', () => {
        expect(calculateMean([1, 3, 5, 7])).toBe(4);
    });
});

describe('calculateMedian', () => {
    test('calculates median correctly', () => {
        expect(calculateMedian([1, 3, 5, 7])).toBe(4);
        expect(calculateMedian([1, 3, 5, 7, 9])).toBe(5); // for odd length array
    });
});

describe('calculateMode', () => {
    test('calculates mode correctly', () => {
        expect(calculateMode([1, 2, 2, 3])).toBe(2);
        expect(calculateMode([1, 1, 2, 3, 3])).toEqual(expect.arrayContaining([1, 3])); // when multiple modes exist
    });
});
